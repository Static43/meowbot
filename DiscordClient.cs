namespace MeowBot
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Threading.Tasks;
  using System.Text;
  using System.IO;
  using Discord;
  using Discord.WebSocket;

  public class DiscordClient
  {
    public DiscordSocketClient SocketClient { get; set; }
    
    public DiscordClient()
    {
      SocketClient = new DiscordSocketClient();
    }
public async Task Login()
{
  var token = File.ReadAllText(".token");
  await SocketClient.StartAsync();
  await SocketClient.LoginAsync(TokenType.Bot, token);
}
public async Task Logout()
{
  await SocketClient.LogoutAsync();
  await SocketClient.StopAsync();
}
  }
}