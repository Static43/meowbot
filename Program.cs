﻿namespace MeowBot
{
  using System;
  using System.Threading.Tasks;
  
  public class Program
  {
    public static async Task Main(string[] args)
    {
      var client = new DiscordClient();
client.SocketClient.MessageReceived += async msg =>
{
  if (msg.Author.Id == client.SocketClient.CurrentUser.Id) return;
  if (msg.Content.ToLower() == "wow")
  {
    await msg.Channel.SendMessageAsync("wow");
  }
  else if (msg.Content.ToLower() == "nook")
  {
    await msg.Channel.SendMessageAsync("https://youtu.be/8pj9th79efI");
  }
};
await client.Login();
Console.ReadLine();
await client.Logout();
    }
  }
}